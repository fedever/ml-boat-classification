import os
import shutil
import numpy as np
from keras.optimizers import Adam, SGD
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Convolution2D, MaxPooling2D, AveragePooling2D
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
import itertools
from keras.preprocessing.image import ImageDataGenerator

np.random.seed(1337) # for reproducibility

categories = {'Public Transport': ['Lanciafino10m', 'Lanciafino10mBianca', 'Lanciafino10mMarrone',
                                   'Lanciamaggioredi10mBianca', 'Lanciamaggioredi10mMarrone',
                                   'Alilaguna', 'VaporettoACTV', 'MotoscafoACTV'],
              'General Transport': ['Motobarca', 'Mototopo', 'Raccoltarifiuti', 'Motopontonerettangolare'],
              'Pleasure Cart': ['Barchino', 'Patanella', 'Sanpierota', 'Cacciapesca', 'Topa'],
              'Rowing Transport': ['Gondola', 'Sandoloaremi', 'Caorlina'],
              'Public Utility': ['Polizia', 'Ambulanza', 'VigilidelFuoco'],
              'None' : ['Water']}


def plot_history(history):
    loss_list = [s for s in history.history.keys() if 'loss' in s and 'val' not in s]
    val_loss_list = [s for s in history.history.keys() if 'loss' in s and 'val' in s]
    acc_list = [s for s in history.history.keys() if 'acc' in s and 'val' not in s]
    val_acc_list = [s for s in history.history.keys() if 'acc' in s and 'val' in s]

    if len(loss_list) == 0:
        print('Loss is missing in history')
        return

    ## As loss always exists
    epochs = range(1, len(history.history[loss_list[0]]) + 1)

    ## Loss
    plt.figure(1)
    for l in loss_list:
        plt.plot(epochs, history.history[l], 'b', label='Training loss (' + str(str(format(history.history[l][-1], '.5f')) + ')'))
    for l in val_loss_list:
        plt.plot(epochs, history.history[l], 'g', label='Validation loss (' + str(str(format(history.history[l][-1], '.5f')) + ')'))

    plt.title('Loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()

    ## Accuracy
    plt.figure(2)
    for l in acc_list:
        plt.plot(epochs, history.history[l], 'b', label='Training accuracy (' + str(format(history.history[l][-1], '.5f')) + ')')
    for l in val_acc_list:
        plt.plot(epochs, history.history[l], 'g', label='Validation accuracy (' + str(format(history.history[l][-1], '.5f')) + ')')

    plt.title('Accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.show()


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    cm = confusion_matrix(y_true, y_pred)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        title = 'Normalized confusion matrix'
    else:
        title = 'Confusion matrix'

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()


def dataset_folder_split():
    # Group the pictures in folders by the label

    # For train set
    for category in categories:
        os.mkdir("TRAIN_SET/" + category)
        for label in categories[category]:
            for file in os.listdir("TRAIN_SET/" + label):
                shutil.move("TRAIN_SET/" + label + "/" + file, "TRAIN_SET/" + category)

    # Than remove all the empty directory
    for folder_name in os.listdir("TRAIN_SET/"):
        if folder_name not in categories.keys() and not folder_name.endswith((".txt", ".DS_Store")):
            os.rmdir("TRAIN_SET/"+folder_name)

    # For test set
    # --- Getting all lines in ground_truth.txt
    fname = 'TEST_SET/ground_truth.txt'
    with open(fname) as f:
        content = f.readlines()
    # --- Building dict {label: [images]}
    pictures_dict = {}
    for row in content:
        name, label = row.split(";")
        label = label.strip()
        label = label.replace(" ", "")
        label = label.replace(":", "")
        # Get corrisponding macrocategory
        for category in categories:
            if label in categories[category]:
                label = category
                break
        # Check if it hasn't got a category
        if label not in categories.keys():
            # Then it must be some sort of labels, like "SnapshotAcqua"
            label = 'None'
        if label not in pictures_dict:
            pictures_dict[label] = []
        pictures_dict[label].append(name)

    # --- Create dir for each label and move file
    for label in pictures_dict:
        os.mkdir("TEST_SET/"+label)
        for filename in pictures_dict[label]:
            shutil.move("TEST_SET/"+filename, "TEST_SET/" + label)


def net(input_shape, num_classes):
    # Initialising the CNN
    model = Sequential()

    # Step 1 - Convolution
    model.add(Convolution2D(filters=32, kernel_size=(3, 3), strides=(2,2), input_shape=input_shape, activation='relu'))

    # Step 2 - Pooling
    model.add(AveragePooling2D(pool_size=(2, 2), strides=(2,2)))

    # Adding a second convolutional layer
    model.add(Convolution2D(filters=32, kernel_size=(3, 3), strides=(2,2), activation='relu'))
    model.add(AveragePooling2D(pool_size=(2, 2)))

    # Step 3 - Flattening
    model.add(Flatten())
    model.add(Dense(128, activation='tanh'))

    # Step 4 - Full connection

    model.add(Dense(units=num_classes, activation='softmax'))

    # Compile
    adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    sgd = SGD(lr=0.01, nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])

    return model


if __name__ == '__main__':
    #dataset_folder_split()
    batch_size = 32

    datagen = ImageDataGenerator(samplewise_std_normalization=True, zoom_range=0.2)

    train_data = datagen.flow_from_directory("TRAIN_SET/",  batch_size=batch_size, class_mode='categorical', target_size=(640, 240),)
    test_data = datagen.flow_from_directory("TEST_SET/", batch_size=batch_size, class_mode='categorical', target_size=(640, 240),)

    # Training Model
    cnn_model = net(train_data.image_shape, train_data.num_classes)
    cnn_model.summary()

    history = cnn_model.fit_generator(train_data,
                            epochs=1, shuffle=True,
                            validation_data=test_data)
    plot_history(history)

    test_steps_per_epoch = np.math.ceil(test_data.samples / test_data.batch_size)

    predictions = cnn_model.predict_generator(train_data, steps=test_steps_per_epoch)

    # Get most likely class
    y_test_pred = np.argmax(predictions, axis=1)

    y_test_true = train_data.classes
    class_labels = list(test_data.class_indices.keys())

    report = classification_report(y_test_true, y_test_pred, target_names=class_labels)
    print(report)

    plot_confusion_matrix(y_test_true, y_test_pred, class_labels)
